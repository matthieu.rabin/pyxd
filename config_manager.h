#pragma once

#define NUMBER_OF_WIFI_NETWORK 6


typedef struct {
	char ssid[32] = { '\0' };
	char pass[64];
} T_wifi_network;

typedef struct {
	uint8_t network_in_use = 0;
	T_wifi_network network[NUMBER_OF_WIFI_NETWORK];
} T_config_wifi_settings;

typedef struct {
	char apikey[64];
	char city[64];
} T_config_weather_settings;

typedef struct {
	int timezone;
	int dst;
} T_config_time_settings;


class ConfigManager{
	public:
		ConfigManager();
		~ConfigManager();

		bool begin();


		
		T_config_wifi_settings    wifi_settings;
		
		T_config_weather_settings weather_settings;
		T_config_time_settings    time_settings;

	private:
		bool init = false;

		bool loadConfig(/* String config_file*/);
		//bool saveConfig();

};

