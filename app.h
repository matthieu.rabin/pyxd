#pragma once

enum E_APP_STATE{
	STATE_INIT = 0,
	STATE_TIME,
	STATE_DAY_MONTH,
	STATE_WEATHER_GET_WEATHER,
	STATE_WEATHER_SHOW_CURRENT_TEMPERATURE,
	STATE_NOP,
	STATE_CRITICAL,
};

struct{
	E_APP_STATE last_state;
	E_APP_STATE current_state;
	E_APP_STATE next_state;
	
	bool state_changed;
	
	uint32_t cycle;
	
	uint32_t lastStatemachineRun;
	
} g_sm = {
	.last_state = STATE_INIT,
	.current_state = STATE_INIT,
	.next_state = STATE_INIT,

	.state_changed = false,
	
	.cycle = 0,
	
	.lastStatemachineRun = 0,
};

const unsigned long subtaskInterval = 100;  
unsigned long lastSubtaskRun = 0;

//typedef bool (*T_sm_handler_handler)(uint32_t);

bool handler_state_init(uint32_t n);
bool handler_state_time(uint32_t n);
bool handler_state_day_month(uint32_t n);
bool handler_state_weather_get_weather(uint32_t n);
bool handler_state_weather_show_current_temperature(uint32_t n);
bool handler_state_nop(uint32_t n);

typedef struct {
	uint32_t periode;
	bool (*handler)(uint32_t);
} T_sm_handler;

T_sm_handler sm_handler[] = {
	{ .periode =  100, .handler = handler_state_init,                             },
	{ .periode = 1000, .handler = handler_state_time,                             },
	{ .periode = 5000, .handler = handler_state_day_month,                        },
	{ .periode = 2000, .handler = handler_state_weather_get_weather,              },
	{ .periode = 5000, .handler = handler_state_weather_show_current_temperature, },
	
	{ .periode =  100, .handler = handler_state_nop,                              },
};



char month[12][5] = {
	"Jan.",
	"Fev.",
	"Mars",
	"Avr.",
	"Mai",
	"Juin",
	"Jul.",
	"Aout",
	"Sep.",
	"Oct.",
	"Nov.",
	"Dec.",
};



