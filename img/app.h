#pragma once
#include <Esp.h>


enum E_APP_STATE{
	STATE_INIT = 0,
	STATE_TIME,
	STATE_WEATHER_GET_WEATHER,
	STATE_WEATHER_SHOW_CURRENT_TEMPERATURE,
	STATE_CRITICAL,
};

typedef struct {
	uint32_t periode;
	bool (*handler)(uint32_t);
} T_sm_handler;


bool handler_state_init(uint32_t n);
bool handler_state_time(uint32_t n);
bool handler_state_weather_get_weather(uint32_t n);
bool handler_state_weather_show_current_temperature(uint32_t n);

T_sm_handler sm_handler[] = {
	{ .periode = 1000,  .handler = handler_state_init,},
	{ .periode = 1000,  .handler = handler_state_time,},
	{ .periode = 2000,  .handler = handler_state_weather_get_weather,},
	{ .periode = 10000, .handler = handler_state_weather_show_current_temperature,},
};

uint32_t cycle = 1;
E_APP_STATE current_state = STATE_INIT;

unsigned long taskSMInterval = 1000;  
unsigned long lastTaskSM = taskSMInterval;


class App{
	
	public:
		App();
		~App();
	
		void begin();
	
		void run();
		
		
	private:

		
		uint32_t cycle = 1;
		E_APP_STATE current_state = STATE_INIT;
		
		unsigned long taskSMInterval = 1000;  
		unsigned long lastTaskSM = taskSMInterval;
}