#include "common.h"
#include "app.h"
#include "pannel.h"
#include "weather.h"
#include "image_data.h"

extern Pannel pannel;
extern Weather weather;

void App::App(){
	
}

void App::~App(){
	
}

void App::run(){
	if (millis() - lastTaskSM > sm_handler[current_state].periode){
		lastTaskSM = millis();
		if(sm_handler[current_state].handler(cycle)){
			cycle++;
			if(cycle == -1) cycle = 10;
		}
	}
}

void changeState(E_APP_STATE newState){
	Serial.printf("[SM] Change state %d -> %d\n",current_state, newState);
	cycle = 0;
	current_state = newState;
	lastTaskSM = 0;
}

bool handler_state_init(uint32_t n){	
	if( n == 1){
		Serial.println("STATE INIT INIT");
	}
	Serial.println("STATE INIT RUN ");
	
	
	if(n>2){
		changeState(STATE_TIME);
	}
	return true;
}

bool handler_state_time(uint32_t n){
	if( n == 1){
		Serial.println("STATE TIME INIT");
		pannel.clear();
	}

	Serial.println("STATE TIME RUN");

	time_t now = time(nullptr);
	struct tm * timeinfo;
	timeinfo = localtime(&now); 
	
	pannel.drawTime(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	pannel.show();
	
	if( n > 60 && (timeinfo->tm_sec < 30)){
		changeState(STATE_WEATHER_GET_WEATHER);
	}
	return true;
}

bool handler_state_weather_get_weather(uint32_t n){
	if(n == 1){
		Serial.println("STATE GET WEATHER"); 
		
		time_t now = time(nullptr);
		struct tm * timeinfo;
		timeinfo = localtime(&now); 
		pannel.clear();
		pannel.drawImageRaw( &img_network);
		pannel.drawTime(timeinfo->tm_hour, timeinfo->tm_min);
		pannel.show();
		
		weather.getWeather();
		
	}else{
		changeState(STATE_WEATHER_SHOW_CURRENT_TEMPERATURE);
	}
	
	return true;
}

bool handler_state_weather_show_current_temperature(uint32_t n){
	if(n == 1){
		Serial.println("STATE SHOW_TEMP"); 
		pannel.clear();
		pannel.drawTemperature(weather.getCurrentTemperature());
		pannel.show();
	}else{
		changeState(STATE_TIME);
	}
	
	return true;
}