#include "system.h"
#include "pannel.h"

#include <ESP8266WiFi.h>

extern Pannel pannel;

void perfMonitor(){
	static uint32_t time = 0;
	static uint32_t counter = 0;
	
	counter++;
	if(millis()-time > 1000){

		Serial.printf("PERF MONITOR, Main loop : %5d Heap : %5d Frag : %3d%% FreeBS : %5d\n", counter, ESP.getFreeHeap(), ESP.getHeapFragmentation(), ESP.getMaxFreeBlockSize());
		
		time = millis();
		counter = 0;
	}
}

void halt( ){
	while(1){
		if(Serial.available()>1){ ESP.restart();}
		delay(1000);
		yield();
	}
}

void critical(E_ERROR_CODE err_code){
	pannel.showError( err_code);
	
	for(int i = 0; i<3; i++){
		digitalWrite(PIN_BUZ,HIGH);
		delay(150);
		digitalWrite(PIN_BUZ,LOW);
		delay(75);
	}
	
	halt();	
}

void WiFiOn() {
  wifi_fpm_do_wakeup();
  wifi_fpm_close();
  wifi_set_opmode(STATION_MODE);
  wifi_station_connect();
}

void WiFiOff() {
  wifi_station_disconnect();
  wifi_set_opmode(NULL_MODE);
  wifi_set_sleep_type(MODEM_SLEEP_T);
  wifi_fpm_open();
  wifi_fpm_do_sleep(0xFFFFFFF);
}
