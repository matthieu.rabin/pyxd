**This is an alpha, i will add some code, documentation, part, etc**
# Pyxel Display
Software, Hardware, Mecanic, I did everything myself with love

//TODO add some photos

# Requirement
Build your own Pyxel Display from scratch

Shopping list
- ESP8266 (Lolin D1 mini )
- 4 or 5 8x8 neoPixel 8x8 pannel
- 1 capa 1000*F 6,3V
- 1 res 330 ohm
- 1 res 10k ohm
- 1 photoresistor
- 3 switchs
- m2 screws (TODO add qtty)
- 3*7cm prototyping PCB

Tools
- Soldering iron
- Computer (obviously)
- 3D printer


(I will add some shopping link)

# Software

## Flash
### Program
> python.exe "C:\Program Files (x86)\Arduino\hardware\espressif\esp8266/tools/upload.py" --chip esp8266 --port COM4 --baud 921600 write_flash 0x0 C:\Users\mattl\AppData\Local\Temp\arduino_build_512815/led_pannel.ino.bin --end

### SPIFFS
**ONLY FOR Loin D1 Mini with 1M of SPIFFS**

> "C:\Program Files (x86)\Arduino\hardware\espressif\esp8266\tools\mkspiffs\mkspiffs.exe" -c "C:\Linux\arduino\led_pannel\data" -p 256 -b 4096 -s 1028096 "C:\Linux\arduino\led_pannel\spiffs.bin"

> python.exe "C:\Program Files (x86)\Arduino\hardware\espressif\esp8266/tools/upload.py" --chip esp8266 --port COM4 --baud 921600 write_flash 0x300000 "C:\Linux\arduino\led_pannel\spiffs.bin" --end 

## Libs

- Esp8266 core for arduino [Github](https://github.com/esp8266/Arduino)

- ArduinoJson 6 https://arduinojson.org/ [Github](https://github.com/bblanchon/ArduinoJson)
- Adafruit_NeoPixel [Github](https://github.com/adafruit/Adafruit_NeoPixel)

## Setup

TODO, edit config.json

### Wifi
-wifi network

### Time
-timezone
-dst

### Weather
-apikey
-city

### Screen


# Hardware
TODO
see /hardware/ to frittzing schematics

# Meca

TODO list each parts with 3Dprinter config



