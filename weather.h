#pragma once

#include "Arduino.h"

typedef struct {
	
} T_weather_data;

class Weather {
	public:
		Weather();
		~Weather();
		
		void begin();
		
		void run();
		
		int8_t getWeather();
		
		bool isInitialized();
		int16_t getCurrentTemperature();
		
		long getSunset();
		long getSunrise();
		
	private:
		bool isInit = false;
		
		//////////////////////////////
		int weather_id = -1;
		
		uint8_t humidity = 0;
		
		int16_t temp = 0;

		
		long sunrise = 0;
		long sunset = 0;
		///////////////////////////////
		
};
