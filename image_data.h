#pragma once
/*
static T_image_raw img_network = {
	.size = 8, //width
	.color = { 0x000000, 0x5fcde4, 0x6abe30}, //Color
	.data = {
		//Haut gauche //Bas gauche
		0x00100000, 
		0x01100000,
		0x11111111,
		0x01100200,
		0x00100220,
		0x22222222,
		0x00000220,
		0x00000200,
		//HAUT doite //Bas droite
	},
};
*/



static T_image_raw img_calendar = {
	.size = 8, //width
	.color = { 0x000000, 0x5fcde4}, //Color
	.data = {
		0x11111111,
		0x01111111,
		0x01111111,
		0x11111111,
		0x11111111,
		0x01111111,
		0x01111111,
		0x11111111,
	},
};


static T_image_raw pika = {
	.size = 8,
	.color = { 0x000000, 0x37474f, 0xffa726, 0xffeb3b, 0xffffff, 0xc41411, 0xc96e1d},
	.data = {
		0x00022000,
		0x10022660,
		0x10000033,
		0x02335322,
		0x00343236,
		0x00333226,
		0x00333232,
		0x12242000,
	},
};



static T_image_raw img_temp_cold = {
	.size = 8,
	//.color = { 0x000000, 0xffffff, 0x03a9f4},
	.color = { 0x000000, 0xffffff, 0x20A0FF},
	.data = {
		0x00000110,
		0x11111021,
		0x10000021,
		0x01111021,
		0x00000110,
		0x00010000,
		0x01010100,
		0x01010100,
	},
};


static T_image_raw img_temp_med = {
	.size = 8,
	.color = { 0x000000, 0xffffff, 0x20FF20},
	//.color = { 0x000000, 0xffffff, 0x42bd41},
	.data = {
		0x00000110,
		0x11111221,
		0x10002221,
		0x01111221,
		0x00000110,
		0x00010000,
		0x01010100,
		0x01010100,
	},
};

static T_image_raw img_temp_hot = {
	.size = 8,
	.color = { 0x000000, 0xffffff, 0xFF2020},
	//.color = { 0x000000, 0xffffff, 0xe51c23},
	.data = {
		0x00000110,
		0x11111221,
		0x10222221,
		0x01111221,
		0x00000110,
		0x00010000,
		0x01010100,
		0x01010100,
	},
};


static T_image_raw img_network = {
	.size = 8,
	.color = { 0x000000, 0x5fcde4, 0x6afe30},
	.data = {
		0x00100000, 
		0x01100000,
		0x11111111,
		0x01100200,
		0x00100220,
		0x22222222,
		0x00000220,
		0x00000200,
	},
};

/*
static T_image img_network = {
	.size = 9,
	.color = { 0x000000, 0x5fcde4, 0x6afe30},
	.data = (uint32_t[]){
		0x00100000, 
		0x01100000,
		0x11111111,
		0x01100000,
		0x00100200,
		0x00000220,
		0x22222222,
		0x00000220,
		0x00000200,
	},
};
*/




