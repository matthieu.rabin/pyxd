#pragma once



#define PIN_BUZ D8

#define PIN_BTN_LEFT  D5
#define PIN_BTN_MID   D6
#define PIN_BTN_RIGHT D7

#define PIN_PHOTOREZ  A0


//LED
#define PIN_LED D1

#define MATRIX_H 8
#define MATRIX_W 32
#define NBR_LED (MATRIX_H*MATRIX_W)


//time
#define TIME_TIMEZONE 2
#define TIME_DST      0

//Weather
#define COLD_LIMIT 18
#define HOT_LIMIT  25


enum E_ERROR_CODE {
	ERROR_NO_ERROR          = 0x00,
	
	ERROR_BOOT_HW_WD,
	ERROR_BOOT_SW_WD,
	ERROR_BOOT_EXCEPTION,
	ERROR_BOOT_DSLP_WAKEUP,
	
	ERROR_SPIFFS_MOUNT_FAILED,
	ERROR_UNKNOWN_ERROR=0xFF,
};


