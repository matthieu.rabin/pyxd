#include <Esp.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <time.h>

#include <Adafruit_NeoPixel.h>

#include "common.h"
#include "pannel.h"
#include "weather.h"
#include "config_manager.h"
#include "system.h"
#include "image_data.h"
#include "app.h"

Pannel pannel;
ConfigManager configManager;
Weather weather;

ESP8266WiFiMulti wifiMulti;

bool lockState = false;


void preinit() {
  // Global WiFi constructors are not called yet
  // (global class instances like WiFi, Serial... are not yet initialized)..
  // No global object methods or C++ exceptions can be called in here!
  //The below is a static class method, which is similar to a function, so it's ok.
  
  //ESP8266WiFiClass::preinitWiFiOff();
}


//TODO TMP
static int btn_counter = 0;

void ICACHE_RAM_ATTR handleInterrupt() {
	Serial.printf("Button : %4d\n",btn_counter++);
}

void setupWiFi(){
	bool ret;
	
	ret = WiFi.setSleepMode( WIFI_MODEM_SLEEP, 10);
	if(!ret) Serial.println( F("WiFi.setSleepMode failed"));
	
	WiFi.setOutputPower(0);	
	//WiFi.setOutputPower(20.5);
	
	WiFi.persistent( false);
	ret = WiFi.mode( WIFI_STA);
	if(!ret) Serial.println( F("WiFi.mode failed"));
	
	ret = WiFi.hostname( "Display");
	if(!ret) Serial.println( F("WiFi.hostname failed"));
	
	for(int i = 0; i < configManager.wifi_settings.network_in_use ; i++){
		wifiMulti.addAP( configManager.wifi_settings.network[i].ssid, configManager.wifi_settings.network[i].pass);
	}
}


void setupSPIFFS() { // Start the SPIFFS and list all contents
	if( !SPIFFS.begin()) {
		Serial.println( F("Failed to mount file system"));
		critical( ERROR_SPIFFS_MOUNT_FAILED);
	}
	Serial.println( F("SPIFFS started."));
}

void bip( void){
	digitalWrite(PIN_BUZ,HIGH);
	delay(100);
	digitalWrite(PIN_BUZ,LOW);
}

void brightnessAction( void){
	if( g_sm.current_state == STATE_TIME){
		changeState( STATE_DAY_MONTH);
		//bip();
	}
}

void setup() {
	Serial.begin(115200);
	delay(2000);
	Serial.println( F("ESP starting."));

	
	pinMode( PIN_BUZ      , OUTPUT);
	pinMode( PIN_BTN_LEFT , INPUT_PULLUP);
	pinMode( PIN_BTN_MID  , INPUT_PULLUP);
	pinMode( PIN_BTN_RIGHT, INPUT_PULLUP);
	
	attachInterrupt(digitalPinToInterrupt(PIN_BTN_LEFT), handleInterrupt, FALLING);
	attachInterrupt(digitalPinToInterrupt(PIN_BTN_MID), handleInterrupt, FALLING);
	attachInterrupt(digitalPinToInterrupt(PIN_BTN_RIGHT), handleInterrupt, FALLING);

	bip();

	pannel.begin();
	pannel.setAutoBrightnessCallback(&brightnessAction);

	const rst_info * resetInfo = system_get_rst_info();

	Serial.print(F("reset reason: "));
	Serial.println(resetInfo->reason);
		
	Serial.print(F("reset reason: "));
	Serial.println(ESP.getResetReason());

	Serial.print(F("Reset Info: "));
	Serial.println(ESP.getResetInfo());
	
	switch(resetInfo->reason){
		case 0: //power on ,vdd
		case 4: //soft restart
		case 6: //external restart
			break;
		case 1: //HW watchdog
			critical( ERROR_BOOT_HW_WD);
		case 2: //Exception reset
			critical( ERROR_BOOT_EXCEPTION);
		case 3: //SW watchdog
			critical( ERROR_BOOT_SW_WD);
		case 5: //DeepSleep wakeup
			critical( ERROR_BOOT_DSLP_WAKEUP);
		default:
			critical( ERROR_UNKNOWN_ERROR);
	}
	
	//Mount fileSystem
	setupSPIFFS();
	
	//Load config
	if(!configManager.begin()){
		//TODO unable to load config
		critical(ERROR_UNKNOWN_ERROR );
	}
	Serial.printf("Timezone : %d\n",configManager.time_settings.timezone);
	
	//Load network settings
	setupWiFi();
	
	weather.begin();
}

void loop() {
	
	//statemachine
	if(millis() - g_sm.lastStatemachineRun > sm_handler[ g_sm.current_state].periode){
		g_sm.lastStatemachineRun = millis();
		if(sm_handler[ g_sm.current_state].handler( g_sm.cycle)){
			g_sm.cycle++;
			if(g_sm.cycle == -1) g_sm.cycle = 10;
		}
		if(g_sm.state_changed){
			Serial.printf("[SM] Change state %d -> %d\r\n", g_sm.current_state, g_sm.next_state);
			g_sm.cycle = 1;
			g_sm.last_state = g_sm.current_state;
			g_sm.current_state = g_sm.next_state;
			g_sm.lastStatemachineRun = 0;
			g_sm.state_changed = false;
		}
	}
	
	
	if(millis() - lastSubtaskRun > subtaskInterval){
		lastSubtaskRun = millis();
		//TODO task Light
		serialLink();
		networkLink();
		pannel.run();
		
	}

	perfMonitor();
}

void changeState(E_APP_STATE next_state){
	if(lockState) return;
	g_sm.next_state = next_state;
	g_sm.state_changed = true;
	

}

bool handler_state_init(uint32_t n){
	if( n == 1){
		Serial.println( F("STATE INIT"));
		pannel.clear();
		pannel.hi();
		pannel.drawImageRaw( &pika);
		pannel.show();
		return true;
	}
	
	pannel.hi();
	pannel.show();
	
	if( n == 2){
		if(wifiMulti.run() != WL_CONNECTED){
			Serial.print(".");
			delay(100);
			return false;
		}else{
			Serial.print( F("WiFi connected to : "));		
			Serial.println(WiFi.SSID()); 
			Serial.print( F("IP address: "));
			Serial.println(WiFi.localIP());
			return true;
		}
	}
	if( n == 3){
		Serial.println( F("\r\nWaiting for time"));
		configTime(configManager.time_settings.timezone * 3600, configManager.time_settings.dst, "pool.ntp.org", "time.nist.gov");
		return true;
	}
	if( n == 4){
		if(time(nullptr) < 946684800){
			Serial.print(".");
			return false;
		}else{
			Serial.println( F("Time set !"));
			time_t now = time(nullptr);
			Serial.println(ctime(&now));
			return true;
		}
	}
	if( n == 5){
		networkLink_setup();
	}
	
	//TODO 	
	if(n > 16){
		changeState(STATE_WEATHER_GET_WEATHER);
	}
	
	return true;
}

bool handler_state_time(uint32_t n){
	if( n == 1){
		Serial.println( F("STATE TIME"));
		pannel.clear();
	}

	time_t now = time(nullptr);
	struct tm timeinfo;

	memcpy(&timeinfo, localtime(&now),sizeof(struct tm));
	
	pannel.drawTime(timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
	makeTimeline();
	pannel.show();


	if( n > 60 && (timeinfo.tm_sec < 30)){
		changeState(STATE_WEATHER_GET_WEATHER);
	}
	return true;
}

bool handler_state_day_month(uint32_t n){
	Serial.println(n);
	if( n == 1){
		Serial.println( F("STATE DAY MONTH"));
		
		time_t now = time(nullptr);
		struct tm* timeinfo;
		timeinfo = localtime(&now);
	
	
		pannel.clear();
		pannel.drawImageRaw( &img_calendar);
		
		if( timeinfo->tm_mday < 10){
			pannel.write3x5char( 3, 2, timeinfo->tm_mday, COLOR_WHITE, false);
		}else{
			pannel.write3x5char( 1, 2, timeinfo->tm_mday/10, COLOR_WHITE, false);
			pannel.write3x5char( 5, 2, timeinfo->tm_mday%10, COLOR_WHITE, false);
		}
		
		Serial.println(month[timeinfo->tm_mon]);
		Serial.println(strlen(month[timeinfo->tm_mon]));
		pannel.writeCharArray( 9, 1, month[timeinfo->tm_mon], strlen(month[timeinfo->tm_mon]));

		pannel.show();
	}else{
		changeState( g_sm.last_state);
	}

	return true;
}

bool handler_state_weather_get_weather(uint32_t n){
	if(n == 1){
		Serial.println( F("STATE GET WEATHER"));
		
		time_t now = time(nullptr);
		struct tm * timeinfo;
		timeinfo = localtime(&now);
		pannel.clear();
		pannel.drawImageRaw( &img_network);
		pannel.drawTime(timeinfo->tm_hour, timeinfo->tm_min);
		pannel.show();
		
		uint8_t ret = weather.getWeather();
		if(ret !=0 ){
			Serial.printf("error weather.getWeather() return %d",ret); 
			changeState(STATE_TIME);
		}
		
	}else{
		changeState(STATE_WEATHER_SHOW_CURRENT_TEMPERATURE);
	}
	
	return true;
}

bool handler_state_weather_show_current_temperature(uint32_t n){
	if(n == 1){
		Serial.println( F("STATE SHOW_TEMP")); 
		pannel.clear();
		pannel.drawTemperature(weather.getCurrentTemperature());
		pannel.show();
	}else{
		changeState(STATE_TIME);
	}
	
	return true;
}

bool handler_state_nop( uint32_t n){
	pannel.test();
	return true;
}
/////////////////////////////////////

void serialLink(){
	int available = Serial.available();
	if (available > 0) {
		char cmd = Serial.read();
		
		switch(cmd){
			case 'A' :
				cmd = Serial.read();
				if(cmd == 'O'){
					Serial.println( "AutoBrightness : Enable");
					pannel.setAutoBrightness( true);
				}else if( cmd == 'F'){
					Serial.println( "AutoBrightness : Disable");
					pannel.setAutoBrightness( false);
				}else{
					Serial.println( "AutoBrightness 'O' or 'F'");
				}
				break;
			case 'B' :
				pannel.setBrightness(Serial.parseInt());
				Serial.println( F("Settings brightness !"));
				break;
			case 'L':
				lockState = !lockState;
				Serial.print( F("LockState : ")); Serial.println(lockState);
				break;
			case 'R':
				Serial.println( F("Rebooting ..."));
				ESP.restart();
				break;
			case 'S':
				Serial.println( F("Change state !"));
				changeState((E_APP_STATE)Serial.parseInt());
			default:
				break;
		}
		
	}
}

void makeTimeline(){
	struct tm * timeinfo;
	
	if(!weather.isInitialized()){
		return;
	}
	
	long sunrise = weather.getSunrise();
	long sunset  = weather.getSunset();
	time_t now = time(nullptr);
	
	timeinfo = localtime(&now);
	uint8_t px_current = ((uint16_t)timeinfo->tm_hour *60 + timeinfo->tm_min)/53.3;
	
	timeinfo = localtime(&sunrise);
	uint8_t px_sunrise = ((uint16_t)timeinfo->tm_hour *60 + timeinfo->tm_min)/53.3;
	
	timeinfo = localtime(&sunset);
	uint8_t px_sunset  = ((uint16_t)timeinfo->tm_hour *60 + timeinfo->tm_min)/53.3;
	

	//Serial.printf("PX : %d, %d, %d\n", px_current, px_sunrise, px_sunset);
	//start 0
	//end   1439
	
	pannel.drawTimeline(px_current,px_sunrise,px_sunset);
	
}

ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80

void handleRoot();              // function prototypes for HTTP handlers
void handleCmd();
void handleNotFound();

void networkLink_setup(){
  server.on("/", HTTP_GET, handleRoot);        // Call the 'handleRoot' function when a client requests URI "/"
  server.on("/", HTTP_POST, handleCmd); // Call the 'handleLogin' function when a POST request is made to URI "/login"
  server.onNotFound(handleNotFound);           // When a client requests an unknown URI (i.e. something other than "/"), call function "handleNotFound"

  server.begin();                            // Actually start the server
  Serial.println( F("[SERVER] HTTP server started"));
}

void networkLink(){
	server.handleClient();                     // Listen for HTTP requests from clients  
	//Serial.println( F("[SERVER] handle client"));
}

void handleRoot() {                          // When URI / is requested, send a web page with a button to toggle the LED
  server.send(200, F("text/html"), F("<form action=\"/\" method=\"POST\"><input type=\"text\" name=\"cmd\" placeholder=\"Commande\"></br></br><input type=\"submit\" value=\"Run\"></form>"));
}

void handleCmd() {                         // If a POST request is made to URI /login
	// If the POST request doesn't have username and password data
	if( ! server.hasArg( "cmd") || server.arg( "cmd") == NULL){
		// The request is invalid, so send HTTP status 400
		server.send( 400, "text/plain", F("400: Invalid Request"));
		return;
	}

	handleRoot();

	String cmd = server.arg( "cmd");
	switch(cmd[0]){
		case 'R':
			Serial.println( F("Rebooting ..."));
			ESP.restart();
			break;
		case 'D':
			Serial.println( F("DeepSleep 10 sec"));
			ESP.deepSleep(10e6);
			break;
		default:
			break;
	}
//server.send(200, "text/html", "<h1>Cmd received : " + server.arg("cmd") + "!</h1><p>Succes !</p>");
}

void handleNotFound(){
	// Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
	server.send(404, "text/plain", F("404: Not found")); 
}

