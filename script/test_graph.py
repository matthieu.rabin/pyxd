from random import *

out =""
out += "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"400\" height=\"150\">\n"
out += "<rect width=\"400\" height=\"150\" fill=\"rgb(200, 200, 200)\" stroke-width=\"1\" stroke=\"rgb(0, 0, 0)\" />\n"
out += "<g stroke=\"black\">\n"
y = randint(0,130)


for x in range(10,390,10):
	y2 = randint(0,130)
	
	out += "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke-width=\"1\" />\n"%( x, 140 - y, x + 10, 140 - y2)
	y = y2;

out += "</g>\n</svg>\n";


with open("out.svg",'w') as f:
	f.write(out)

import webbrowser
webbrowser.open("out.svg")