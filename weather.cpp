
#include "weather.h"
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>


//TODO REMOVE THIS:
#include <time.h>
#include "config_manager.h"

extern ConfigManager configManager;

Weather::Weather(){
	
}

Weather::~Weather(){
	
}

void Weather::begin(){
	
}

int8_t Weather::getWeather(){
	WiFiClient client;
	client.setTimeout(10000);
	if( !client.connect(F("api.openweathermap.org"), 80)) {
		Serial.println(F("Connection failed"));
		return -1;
	}

	Serial.println(F("Connected!"));
	
	//make request string
	String data;
	data.reserve(256);
	
	data = F("GET /data/2.5/weather?q=");
	data += configManager.weather_settings.city;
	data += F("&APPID=");
	data += configManager.weather_settings.apikey;
	data += F("&mode=json&units=metric HTTP/1.1\r\n");
	data += F("Host: api.openweathermap.org\r\n");
	data += F("User-Agent: ArduinoWiFi/1.1\r\n");
	data += F("Connection: close\r\n\r\n");
	
	if( client.println(data) == 0) {
		Serial.println(F("Failed to send request"));
		return -2;
	}	

	// Check HTTP status
	char status[32] = {0};
	client.readBytesUntil('\r', status, sizeof(status));
	if( strcmp(status, "HTTP/1.1 200 OK") != 0) {
		Serial.print(F("Unexpected response: "));
		Serial.println(status);
		return -3;
	}

	// Skip HTTP headers
	char endOfHeaders[] = "\r\n\r\n";
	if( !client.find(endOfHeaders)) {
		Serial.println(F("Invalid response"));
		return -4;
	}

	// Allocate the JSON document
	// Use arduinojson.org/v6/assistant to compute the capacity.
	const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(1) + 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(6) + JSON_OBJECT_SIZE(12) + 270;
	DynamicJsonDocument doc(2048);

	// Parse JSON object
	DeserializationError error = deserializeJson(doc, client);
	if( error) {
		Serial.print(F("deserializeJson() failed: "));
		Serial.println(error.c_str());
		return -5;
	}
	
	JsonObject weather_0 = doc["weather"][0];
	if( weather_0.isNull()) return -6;
    weather_id = weather_0["id"]; // 803

    JsonObject main = doc["main"];
	if( main.isNull()) return -7;
    float temperature = main["temp"]; // 278.16
	temp = temperature+0.5; //round temperature
	
    humidity = main["humidity"]; // 69

	
    JsonObject sys = doc["sys"];
	if( sys.isNull()) return -8;
	
    sunrise = sys["sunrise"]; // 1553052784
    sunset = sys["sunset"]; // 1553096472

	//fix timezone
	sunrise += configManager.time_settings.timezone * 3600;
	sunset  += configManager.time_settings.timezone * 3600;
	
	
	//TODO RM
	struct tm * timeinfo;
	timeinfo = localtime(&sunrise); 
	printf("Sunrise : %02dh %02dm %02ds\n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	
	timeinfo = localtime(&sunset); 
	printf("Sunset : %02dh %02dm %02ds\n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

	

    printf("Memory usage JSON : %d/%d\n", doc.memoryUsage(), capacity);
	
	isInit=true;
	// Disconnect
	
	client.stop();
	return 0;
}

void Weather::run(){
	
}

bool Weather::isInitialized(){
	return isInit;
}

int16_t Weather::getCurrentTemperature( void){
	return temp;
}

long Weather::getSunrise(){
	return sunrise;
}

long Weather::getSunset(){
	return sunset;
}
