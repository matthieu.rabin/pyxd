#pragma once

#include <Adafruit_NeoPixel.h>

#include "common.h"

#define OFF 0

#define COLOR_RED    0xFF0000
#define COLOR_BLUE   0x0000FF
#define COLOR_GREEN  0x00FF00
#define COLOR_WHITE  0xFFFFFF
#define COLOR_YELLOW 0xFFFF00
#define COLOR_ORANGE 0xFFA500
#define COLOR_PINK   0xFF007F
#define COLOR_CYAN   0x00FFFF
#define COLOR_PURPLE 0x7F00FF


#define COLOR_SUNRISE 0x00D8FF
#define COLOR_SUNSET  0xFFB000
#define COLOR_DAY     0xFFFFFF
#define COLOR_NIGHT   0x666666

typedef struct {
	uint16_t size;
	uint32_t color[16];
	uint32_t data[8];
} T_image_raw;

class Pannel{
	public:
		Pannel();
		~Pannel();

		void begin();
		void show();
		void clear();

		void hi(void);

		void setAutoBrightnessCallback( void (* autoBrightness_Callback)(void));
		void setAutoBrightness( bool autoBrightness);
		
		void setBrightness( uint8_t brightness);
		void setPixelColor( int x, int y, uint32_t color);
		uint8_t writeChar( int x_offset, int y_offset, char c, uint32_t color=COLOR_WHITE, bool background=true, uint32_t bg_color=0);
		void writeString( int x_offset, int y_offset, String str, uint32_t color=COLOR_WHITE, bool background=true, uint32_t bg_color=0);
		void writeCharArray( int x_offset, int y_offset, char* str, size_t length, uint32_t color=COLOR_WHITE, bool background=true, uint32_t bg_color=0);

		///////// TEST
		void toggle( void);
		void test( void);
		////////

		void drawTime( uint8_t hh, uint8_t mm);
		void drawTime( uint8_t hh, uint8_t mm, uint8_t ss);
		void drawTemperature( int16_t temp, bool inCelcius = true);
		void drawImageRaw( T_image_raw* img, int8_t x_offset=0, int8_t y_offset=0);
		void drawTimeline( uint8_t px_current, uint8_t px_sunrise, uint8_t px_sunset);

		void showError( E_ERROR_CODE error );

		void run( void);

		void write3x5char( int x_offset, int y_offset, uint8_t c, uint32_t color=COLOR_WHITE, bool background=true, uint32_t bg_color=0);

		void makeGradient( uint32_t* gradient, uint8_t size, uint32_t color1, uint32_t color2);

	private:
		Adafruit_NeoPixel strip = Adafruit_NeoPixel( NBR_LED, PIN_LED, NEO_GRB + NEO_KHZ800);

		
		uint32_t Wheel( byte WheelPos);
		uint8_t brightness = 5;

		bool     autoBrightness = true;
		uint16_t autoBrightness_Array[16] = {0};
		uint16_t autoBrightness_Value = 0;
		uint8_t  autoBrightness_Index = 0;
		
		uint16_t autoBrightness_VariationThreshold = 256;
		bool     autoBrightness_Debounce = false;
		void   (*autoBrightness_Action)( void);
		
};

