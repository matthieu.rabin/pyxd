#pragma once

#include "common.h"

void halt();
void perfMonitor();
void critical(E_ERROR_CODE err_code);