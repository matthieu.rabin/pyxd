#include <ArduinoJson.h>
#include <FS.h>

#include "config_manager.h"


ConfigManager::ConfigManager(){


}
ConfigManager::~ConfigManager(){


}

bool ConfigManager::begin( void){
	//TODO check config
	
	/*
	
	if(SPIFFS.exists()){
		
		
	}
	
	if(loadConfig("\config.json")){
		return true;
	}
	
	if(loadConfig("\config_backup.json"){
		//todo cp config_backup to config
		return true;
	}*/
	
	//generate new config
	//saveConfig("\config.json");
	
	if(loadConfig()){
		return true;
	}
	
	return false;
}

bool ConfigManager::loadConfig(/* String config_file*/){
	
	File configFile = SPIFFS.open("/config.json", "r");
	if (!configFile) {
		Serial.println("Failed to open config file");
		return false;
	}
	size_t size = configFile.size();
	if (size > 1024) {
		Serial.println("Config file size is too large");
		return false;
	}

	std::unique_ptr<char[]> buf(new char[size]);
	
	configFile.readBytes(buf.get(), size);
	
	
	const size_t capacity = JSON_ARRAY_SIZE(5) + 6*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 680;
	DynamicJsonDocument doc(capacity);

	
	DeserializationError error = deserializeJson(doc, buf.get());
	if( error) {
		Serial.print(F("deserializeJson() failed: "));
		Serial.println(error.c_str());
		return false;
	}
	

	JsonObject time = doc["time"];
	bool time_enabled = time["enabled"];
	time_settings.timezone = time["timezone"];
	time_settings.dst = time["dst"];

	JsonObject weather = doc["weather"];
	bool weather_enabled = weather["enabled"];
	const char* weather_city = weather["city"];
	const char* weather_apikey = weather["apikey"];
	int weather_period = weather["period"];

	//TODO make secure strncpy
	strncpy( weather_settings.city, weather_city, sizeof(weather_settings.city));
	strncpy( weather_settings.apikey, weather_apikey, sizeof(weather_settings.apikey));

	JsonArray wifi_network = doc["wifi_network"];
	wifi_settings.network_in_use = 0;
	for(uint8_t i = 0; i < NUMBER_OF_WIFI_NETWORK && i < wifi_network.size(); i++){
		strncpy( wifi_settings.network[i].ssid, wifi_network[i]["ssid"], sizeof(wifi_settings.network[i].ssid));
		strncpy( wifi_settings.network[i].pass, wifi_network[i]["pass"], sizeof(wifi_settings.network[i].pass));
		wifi_settings.network_in_use++;
	}

	return true;
}