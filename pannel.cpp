#include "pannel.h"
#include "font5x7.h"
#include "font3x5.h"

#include <Adafruit_NeoPixel.h>

#include "image_data.h"


Pannel::Pannel(){

}

Pannel::~Pannel(){
	
}

void Pannel::begin( void){
	this->strip.begin();
	this->strip.setBrightness(brightness);
	this->strip.clear();
	this->strip.show();
}

void Pannel::setBrightness(uint8_t brightness){
	if( brightness > 30) brightness = 30; // With great power, comes great responsibility (and so much Amps)
	this->strip.setBrightness( brightness);
	//this->brightness = brightness;
}

void Pannel::setAutoBrightnessCallback( void (* autoBrightness_Callback)(void)){
	this->autoBrightness_Action = autoBrightness_Callback;
}

void Pannel::setAutoBrightness( bool autoBrightness){
	this->autoBrightness = autoBrightness;
}

void Pannel::show( void){
	//TODO 
	//this->strip.setBrightness( brightness);
	this->strip.show();
}

void Pannel::clear( void){
	this->strip.clear();
}

void Pannel::hi( void){
	static uint8_t cycle = 0;
	cycle+=16;
	
	uint32_t color = Wheel((cycle) & 255);
	writeString(10,1,"HI!", color);
}

/*
void Pannel::hi( void){
	writeString(10,1,"HI!", COLOR_WHITE);
}*/


void Pannel::test( void){
	static uint8_t j = 0;
	for(int i=0; i< strip.numPixels(); i++) {
		setPixelColor(i/8,i%8, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
	}
	j+=16;
	show();
}

void Pannel::toggle( void){
	static bool enable = false;
	enable =! enable;
	setPixelColor(0,7, enable ? COLOR_BLUE : OFF);
	show();
}

void Pannel::showError( E_ERROR_CODE err_code){
	char buffer[5] = {'\0'};
	
	clear();
	
	//TODO display error logo 8x8
	
	strip.fill( COLOR_RED, 0 , 64);
	
	snprintf( buffer, sizeof(buffer), "0x%02X", err_code);
	String str_time((const __FlashStringHelper*) buffer);
	writeString(9,1,str_time);	
	
	show();
}

void Pannel::drawImageRaw( T_image_raw* img, int8_t x_offset, int8_t y_offset){
	if( img == NULL){
		Serial.println("drawImageRaw null pointer");
		return;
	}

	for( int x = 0; x < img->size; x++){
		uint32_t data = img->data[x];

		for( int y = 0; y < 8; y++){
			uint8_t color_id = (data >> ((7-y)*4)) & 0xF;
			setPixelColor(x+x_offset,y+y_offset,img->color[color_id]);
		}
	}
}

void Pannel::setPixelColor( int x, int y, uint32_t color){
	//Serial.printf("x:%02d, y:%02d, x:%06x\n",x,y,color);
	if( x >= MATRIX_W || y >= MATRIX_H || x < 0 || y < 0) return; // OOB exceptions
	
	uint8_t pannel = x/8;
	x = x%8;

	uint16_t pixel_id = pannel*64 + 8 * y + x;
	
	strip.setPixelColor( pixel_id, color);	
}


uint8_t Pannel::writeChar( int x_offset, int y_offset, char c, uint32_t color, bool background, uint32_t bg_color){
	
	// TODO check if char is on array
	for( int x=0; x<5; x++){
		unsigned char col = pgm_read_byte(&Font5x7[((c - 0x20) * 5) + x]);
		
		for( int y=0; y < 7 ; y++){
			if( (col>>y)&1 ){
				setPixelColor( x_offset+x, y_offset+y, color);
			}else{
				if(background) setPixelColor( x_offset+x, y_offset+y, bg_color);
			}
		}
	}
	
	return 0;
}

void Pannel::writeString( int x_offset, int y_offset, String str, uint32_t color, bool background, uint32_t bg_color){
	int length = str.length();
	for(int i=0; i< length; i++){
		char c = str.charAt(i);
		writeChar(x_offset + 6*i , y_offset, c, color, background, bg_color);
		
		for(int j = 0; j <8; j++){
			this->setPixelColor(x_offset-1 + 6*(i+1),y_offset+j,0);
		}
		
	}
}

void Pannel::writeCharArray( int x_offset, int y_offset, char* str, size_t length, uint32_t color, bool background, uint32_t bg_color){
	
	for(int i=0; i< length; i++){
		writeChar(x_offset + 6*i , y_offset, str[i], color, background, bg_color);
		
		for(int j = 0; j <8; j++){
			this->setPixelColor(x_offset-1 + 6*(i+1),y_offset+j,0);
		}
		
	}
	
}

void Pannel::write3x5char(  int x_offset, int y_offset, uint8_t char_id, uint32_t color, bool background, uint32_t bg_color){
	for( int x=0; x<3; x++){
		unsigned char col = pgm_read_byte(&Font3x5[((char_id) * 3) + x]);
		
		for( int y=0; y < 5 ; y++){
			if( col&(1<<y) ){
				setPixelColor( x_offset+x, y_offset+y, color);
			}else{
				if(background) setPixelColor( x_offset+x, y_offset+y, bg_color);
			}
		}
	}
}

void Pannel::drawTime( uint8_t hh, uint8_t mm){
	uint8_t time[4];
	
	time[0] = hh/10;
	time[1] = hh%10;
	
	time[2] = mm/10;
	time[3] = mm%10;
	
	uint8_t x_offset = 10;
	uint8_t y_offset = 1;

	write3x5char( x_offset   , y_offset, time[0]);
	write3x5char( x_offset+4 , y_offset, time[1]);
	
	setPixelColor(x_offset+8 , y_offset+1, COLOR_WHITE);
	setPixelColor(x_offset+8 , y_offset+3, COLOR_WHITE);
	
	write3x5char( x_offset+10, y_offset, time[2]);
	write3x5char( x_offset+14, y_offset, time[3]);
}

void Pannel::drawTime( uint8_t hh, uint8_t mm, uint8_t ss){	
	uint8_t x_offset = 2;
	uint8_t y_offset = 1;

	write3x5char( x_offset   , y_offset, (uint8_t)hh/10);
	write3x5char( x_offset+4 , y_offset, (uint8_t)hh%10);
	
	setPixelColor(x_offset+8 , y_offset+1, COLOR_WHITE);
	setPixelColor(x_offset+8 , y_offset+3, COLOR_WHITE);
	
	write3x5char( x_offset+10, y_offset, (uint8_t)mm/10);
	write3x5char( x_offset+14, y_offset, (uint8_t)mm%10);
	
	setPixelColor(x_offset+18 , y_offset+1, COLOR_WHITE);
	setPixelColor(x_offset+18 , y_offset+3, COLOR_WHITE);
	
	write3x5char( x_offset+20, y_offset, (uint8_t)ss/10);
	write3x5char( x_offset+24, y_offset, (uint8_t)ss%10);
	
}

void Pannel::drawTemperature( int16_t temp, bool isCelsius){
	uint8_t x_offset = 9;
	uint8_t y_offset = 1;

	bool isNegative = temp < 0;
	if( isNegative) temp = - temp;
	bool isOneDigit = temp < 10;
	
	if(temp > 99) temp = 99;
	
	if( temp < COLD_LIMIT || isNegative){
		drawImageRaw( &img_temp_cold);
	}else if (temp < HOT_LIMIT){
		drawImageRaw( &img_temp_med);
	}else{
		drawImageRaw( &img_temp_hot);
	}
	
	if( isOneDigit){
		if( isNegative){
			setPixelColor(x_offset+2   , y_offset+2, COLOR_WHITE);
			setPixelColor(x_offset+3 , y_offset+2, COLOR_WHITE);
		}
		x_offset-=2;
	}else{
		if( isNegative){
			setPixelColor(x_offset   , y_offset+2, COLOR_WHITE);
			setPixelColor(x_offset+1 , y_offset+2, COLOR_WHITE);
		}
		write3x5char( x_offset+4, y_offset, (uint8_t)(temp/10));
	}
	
	write3x5char( x_offset+8, y_offset, (uint8_t)(temp%10));
	
	setPixelColor(x_offset+12 , y_offset, COLOR_WHITE);
	setPixelColor(x_offset+12 , y_offset+1, COLOR_WHITE);
	setPixelColor(x_offset+13 , y_offset, COLOR_WHITE);
	setPixelColor(x_offset+13 , y_offset+1, COLOR_WHITE);
	
	write3x5char(x_offset+15, y_offset, (isCelsius ? 10 : 11));	
}

/*
void write_string_loop(String str){
	static int c = - MATRIX_W;
	
	int length = str.length()*6;
	c += 1;
	
	if(c > length){
		c = -MATRIX_W;
	}
	Serial.printf("Ca:%d\n",c);
	
	write_string(0-c,1,str);
}*/

uint32_t Pannel::Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3,0);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3,0);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0,0);
}

void Pannel::run(void){
	if( !autoBrightness){
		return;
	}
	
	uint16_t sample = analogRead(PIN_PHOTOREZ);
	
	autoBrightness_Value = autoBrightness_Value + sample - autoBrightness_Array[autoBrightness_Index];
	
	
	autoBrightness_Array[autoBrightness_Index] = sample;
	autoBrightness_Index++;
	autoBrightness_Index &= 16 - 1 ;
	
	//Serial.printf("[ADC] IDX : %2d, Sample : %4d, brightnessValue : %4d\n", autoBrightness_Index, sample, autoBrightness_Value>>4);
	
	if( sample < (autoBrightness_Value>>4) - autoBrightness_VariationThreshold){
		if( (autoBrightness_Action != NULL) && (autoBrightness_Debounce == false)){
			autoBrightness_Debounce = true;
			autoBrightness_Action();
		}
	}else{
		autoBrightness_Debounce = false;
	}
	
	uint16_t autoBrightness_brightness = (autoBrightness_Value>>4);
	if( autoBrightness_brightness < 100){
		setBrightness(1);
	}else if ( autoBrightness_brightness < 200){		
		setBrightness(2);
	}else if ( autoBrightness_brightness < 300){		
		setBrightness(5);
	}else if ( autoBrightness_brightness < 700){
		setBrightness(10);
	}else if ( autoBrightness_brightness < 900){
		setBrightness(20);
	}else{
		setBrightness(30);
	}
	
	/*
	char str[6];
	sprintf(str, "%5d", autoBrightness_Value >> 4);
	
	writeString( 3,1, String(str));
	*/

	uint8_t px = autoBrightness_brightness >> 5;
	for(int i=0; i< 32;i++){
		if( i <= px){
			setPixelColor(i,0, COLOR_PINK);
		}else{
			setPixelColor(i,0,OFF);
		}
	}
	show();
}

void Pannel::drawTimeline( uint8_t px_current, uint8_t px_sunrise, uint8_t px_sunset){
	#define TIMELINE_WIDTH 27
	uint8_t grad_range = (px_sunset-px_sunrise)+1;


	uint32_t* gradient = (uint32_t *)malloc( sizeof( uint32_t) * TIMELINE_WIDTH );
	if(gradient == NULL){
		Serial.println("Malloc failed ! ");
		return;
	}


	makeGradient( &gradient[0]          ,px_sunrise                 , COLOR_PURPLE, COLOR_CYAN);
	makeGradient( &gradient[px_sunrise] ,(px_sunset-px_sunrise) + 1 , COLOR_YELLOW, COLOR_RED);
	makeGradient( &gradient[px_sunset]  ,TIMELINE_WIDTH - px_sunset , COLOR_CYAN , COLOR_PURPLE);


	for(int i=0; i < TIMELINE_WIDTH ;i++ ){
		setPixelColor(2+i,7, gradient[i]);
	}
	setPixelColor(2+px_current,7, COLOR_WHITE);
	
	free(gradient);
}

void Pannel::makeGradient(uint32_t* gradient, uint8_t size ,uint32_t color1, uint32_t color2){
	if( gradient == NULL){
		return;
	}
	if(size<2) return;
	
	uint8_t bR = ((color1 >> 16) & 0xFF );
	uint8_t bG = ((color1 >> 8) & 0xFF );
	uint8_t bB = ((color1) & 0xFF );
	
	int16_t dR = ((color2 >> 16) & 0xFF) - bR;
	int16_t dG = ((color2 >> 8) & 0xFF) - bG;
	int16_t dB = ((color2) & 0xFF) - bB;
	
	for(int i = 0; i < size; i++){
		float offset = (i/(float)(size-1));
		uint8_t cR = bR + dR*offset;
		uint8_t cG = bG + dG*offset;		
		uint8_t cB = bB + dB*offset;
		
		gradient[i] = ((cR & 0xFF) << 16) + ((cG & 0xFF) << 8) + ((cB & 0xFF));
	}
}

//

//

/*
void rainbowCycle(uint16_t wait) {
	static uint8_t j=0;
	static uint32_t time=0;
	uint16_t i;
	

	if(millis()-time < wait)return;
	
	for(i=0; i< pannel.strip.numPixels(); i++) {
	
		if(random(0,10) > 5){
			setPixelColor(i/8,i%8, 0);
		}else{
			setPixelColor(i/8,i%8, Wheel(((i * 256 / pannel.strip.numPixels()) + j) & 255));
		}
		
		//pannel.strip.setPixelColor(i, Wheel(((i * 256 / pannel.strip.numPixels()) + j) & 255));
	}
	pannel.strip.show();
	j++;
	
	time=millis();
}*/
